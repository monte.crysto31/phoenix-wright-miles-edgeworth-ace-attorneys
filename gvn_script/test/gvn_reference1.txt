# this is a comment
// this is also a comment

# empty lines do nothing


LABEL Test1
	
	JUMP Scenes

	# this is how a character speaks
	char1
	-- A line with only one word that is not a known and reserved keyword is considered a Character handle (single-word shortened name by which we identify Character objects)

	char2
	-- like this
	-- All lines onwards starting with a double dash "--" will be said by this Character, until a new non-empty line starts with another name or keyword
	-- Empty lines are ignored and do not interrupt a speaker, and all whitespaces at the beginning and end are ignored so indentation can be added for ease of reading
	
	JUMP Label_Name

END


# blocks of scripts can be opened and closed with the LABEL and END keywords
LABEL Label_Name

	char1
	-- Basically this allows us to jump to a certain position in the script
	-- We can use the keywords JUMP and CALL the same way as in Renpy
	-- Indentation is still optional but highly recommended.
	
	CALL Label_Call
	
	char2
	-- Toto 4
	
	char2
	-- The system will keep track of every position when a label is called in order to go back to it when said label ends
	-- This will work as intended when jumping to and calling labels inside of other calls and jumps because these positions will be stored in a heap and taken out as needed
	 
	JUMP Conditions

END


LABEL Label_Call

	char2
	-- Toto 1
	
	char1
	-- Toto 2
	
	CALL Label_Call2

END


LABEL Label_Call2

	char1
	-- Toto 3

END


# We can evaluate contitions (==, !=, <, =<, >, =>) and set values on variables
# declared in the Globals sigleton (case-sensitive, spaces are mandatory
# between operators). You can also force a certain type with bool() of float()

LABEL Conditions

	IF Test < 3
		
		char1
		-- Test is strictly lower than 3
		
		IF Test == 1
			
			char1
			-- Test is equal to 1
		
		ELSE
			
			char1
			-- Test is not equal to 1
		
		ENDIF
	
	ELSE
	
		char2
		-- Test is greater than or equal to 3, substrating 3 from it
		
		SET Test -= 3
		
		IF Test < 3
			
			char1
			-- Test is now strictly lower than 3
			
			IF Test >= 2
			
				char1
				-- Test is greater than or equal to 2
			
			ENDIF
		
		ELSE
			
			char1
			-- Test is still greater than or equal to 3
		
		ENDIF
	
	ENDIF
	
	char1
	-- Finishing label Conditions
	
	JUMP Scenes

END


LABEL Scenes
	
	SCENE court_desk_prosecution
	SHOW EdgCourtOof
	SHOW EdgCourtOof AT (500,500)
	
	edg
	-- A scene A scene A scene A scene
	
	TRANSITION CourtSlide EdgCourtAngry WrightCourtThink rl

	wri
	-- And now a transition to a new one! And now a transition to a new one! And now a transition to a new one! And now a transition to a new one!
	
	TRANSITION  UmiBreakup   1
	SCENE office_night
	SHOW EdgCourtIdle
	
	edg
	-- Tadaaaaa!
	
	CALL Jump_Scripts
	
	FINISH

END


# we present decisions with the CHOICE keyword
CHOICE
-- This is the first choice  // Label_Choice1
-- This is the second choice // Label_Choice2
