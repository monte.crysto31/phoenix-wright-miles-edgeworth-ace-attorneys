extends Node


# dialogue

signal say(who:Character, what:String)
signal next_line


# scene manipulation

signal scene(new_scene:String)
signal show (new_image:String, at:String, with:String)

signal transition(trans:String, args:Array[String])
signal transition_started
signal transition_finished

signal camera_span(x1:float, x2:float, dur:float)
