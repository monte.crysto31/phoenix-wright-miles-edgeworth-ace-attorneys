extends Node

# characters and speech
var SPEAKER          : Character  = null
var SPEAKING         : bool       = false
var CHARACTERS       : Dictionary = {} # dictionary of name -> Character reference
var _CHARACTERS_FILE : String     = "res://gvn_script/characters.json"

# animations we have to wait for
var RUNNING_ANIMATIONS : Array[int] = []

var  HISTORY       : Array = [] # array of id -> [who, what, color]
var _HISTORY_COUNT : int   = 0
var _HISTORY_LIMIT : int   = 1000

# GVN script files
var SCRIPT        : GVN_Script = null
var SCRIPT_NAME   : String     = ""
var SCRIPTS       : Dictionary = {} # name -> GVN_Script reference

# stacks used to go back and continue from after the previous CALL or IF/ELSE/ENDIF statement
var CALL_STACK  : Array = [] # array of [script_name, pos]
var IF_STACK    : Array = [] # array of [script_name, pos]


signal script_ended


func _ready():
	
	SignalBus.say.connect(character_say)
	
	var json : JSON = JSON.new()
	
	json.parse(FileAccess.get_file_as_string(_CHARACTERS_FILE))
	
	for key in json.data:
		self.CHARACTERS[key] = Character.new(key, json.data[key])
	
	var tmp_labels : Dictionary = {}
	
	for script_name in Utils.get_all_files("res://gvn_script", ".txt"):
		
		self.SCRIPTS[script_name] = GVN_Script.new(script_name)
		
		for label in self.SCRIPTS[script_name].LABELS.keys():
			
			if tmp_labels.keys().has(label):
				printerr("Attempting to define label '", label, "' twice:\n",
						 " - Exists in script    '", tmp_labels[label], "'\n",
						 " - Redefined in script '", script_name, "'\n")
				return
				
			else:
				tmp_labels[label] = script_name


func START(label_name:String):

	jump_to_label(label_name)
	play_line()


func goto_script(script_name:String):
	
	if not self.SCRIPTS.has(script_name):
		self.SCRIPT = null
		printerr("Could not find GVN script '", script_name, "'")
		return
		
	self.SCRIPT = self.SCRIPTS[script_name]


# returns the name of the script where the label was found
func goto_label(label_name:String):
	
	for script_name in SCRIPTS:
		if self.SCRIPTS[script_name].LABELS.has(label_name):
		
			goto_script(script_name)
			self.SCRIPT_NAME = script_name
			self.SCRIPT.pos = self.SCRIPT.LABELS[label_name]
			
			return
	
	printerr("Could not find label '", label_name, "' in any script file")
	return


# jump to the line following the requested LABEL so we can start the script from there
# this will work as intended event if the label is empty since the END statement will
# be at least one line after the LABEL statement 
func jump_to_label(label_name:String):
	
	self.CALL_STACK = []
	self.IF_STACK   = []
	
	goto_label(label_name)


# remember where to go back to when finding the END keyword and jump to the called label
func call_label(label_name:String):
	
	self.CALL_STACK.push_front([self.SCRIPT_NAME, self.SCRIPT.pos])
	goto_label(label_name)


func end_label():
	
	if self.CALL_STACK.size() == 0:
		
		GvnPlayer.end_script()
		return
	
	else:
		
		var last_call = self.CALL_STACK.pop_front()
		goto_script(last_call[0])
		self.SCRIPT.pos = last_call[1]


func end_script():
	
	self.SCRIPT = null
	script_ended.emit()


func character_say(who:Character, what:String):
	
	# update speaking character
	self.SPEAKER = CHARACTERS[who.short_name]
	
	# add line to the HISTORY
	self.HISTORY.push_back([
		who.char_name,
		what,
		who.what_color])
	
	self._HISTORY_COUNT += 1
	
	if self._HISTORY_COUNT > self._HISTORY_LIMIT:
		self.HISTORY.pop_front()
		self._HISTORY_COUNT -= 1
	
	# debug
	#print("%s:\n\t%s\n" % [who.char_name, what])


# parse an expression for a SET or IF statement
func parse_expression(expr_str:String, condition:bool) -> Variant:
	
	var script := GDScript  .new()
	var ref    := RefCounted.new()
	var expr   := expr_str
	
	# parse expression in expr_str
	for token in expr_str.split(" "):
		
		if token in Globals:
			expr = expr.replace(token, "Globals.%s" % token)
	
	if condition:
		script.set_source_code("func eval(): return %s" % expr)
	else:
		script.set_source_code("func eval(): %s" % expr)
	
	script.reload()
	
	ref.set_script(script)
	
	return ref.eval()


func play_line():
	
	while true:
		
		SCRIPT.file.seek(SCRIPT.pos)
		
		var line : String = ""
		
		if SCRIPT.file.eof_reached():
			break
		
		# advance a line in the script
		line       = SCRIPT.file.get_line().strip_edges()
		SCRIPT.pos = SCRIPT.file.get_position()
		
		# we play the next line if the current one is empty or a comment
		if line.is_empty() or line.begins_with("#") or line.begins_with("//"):
			continue
		
		# if not, then we play the line we got
		
		var keyword : String = line.get_slice(" ", 0)
		var content : String = line.trim_prefix(keyword).strip_edges()
		keyword = keyword.to_lower()
		
		# are we transitioning from one scene to another, and if yes is the next line
		# something other than a SCENE or SHOW statement (we have shown averything in
		# the new scene and now need to wait for the transition to finish in order to
		# continue)?
		if SCRIPT.TRANS and not (keyword == "scene" or keyword == "show"):
			await SignalBus.transition_finished
			print("Finished transition")
			SCRIPT.TRANS = false
			# continue as normal
		
		# If we're done showing the scene, wait for a potential blocking animation to finish
		if not (keyword == "scene" or keyword == "show"):
			while RUNNING_ANIMATIONS.size() != 0:
				await get_tree().create_timer(0.01).timeout
		
		# are we in a tirade right now, and if yes is the new line a line of dialogue?
		SCRIPT.TIRADE = SCRIPT.TIRADE and line.begins_with("-- ")
		
		if SCRIPT.TIRADE:
			SignalBus.say.emit(CHARACTERS[SCRIPT.SPEAKER], line.erase(0, 3).strip_edges())
			await SignalBus.next_line
			continue
		
		# the statement is of the form "KEYWORD CONTENT"
		match keyword:
			
			"jump":
				print("Jumping to label %s\n" % content)
				jump_to_label(content)
			
			"call":
				print("Calling label %s\n" % content)
				GvnPlayer.call_label(content)
			
			"end":
				print("Ending label\n")
				GvnPlayer.end_label()
			
			"choice":
				printerr("TODO (GVN): choice mechanic\n")
				return
			
			"if":
				var res = parse_expression(content, true)
				
				IF_STACK.push_front(SCRIPT.pos)
				
				# we do nothing if the condition is true, we will play the next line and that's it
				# but if it's false, we either jump to ELSE if there's one, or to ENDIF
				if not res:
					
					if SCRIPT.CONDITIONS[IF_STACK[0]].has("else"):
						SCRIPT.pos = SCRIPT.CONDITIONS[IF_STACK[0]]["else"]
					
					# if we go to ENDIF, then we pop one IF fom the heap to close it
					else:
						SCRIPT.pos = SCRIPT.CONDITIONS[IF_STACK[0]]["endif"]
						IF_STACK.pop_front()
			
			# if we encounter an ELSE, then we came from an IF and thus need to jump
			# to ENDIF since the condition was true
			"else":
				SCRIPT.pos = SCRIPT.CONDITIONS[IF_STACK[0]]["endif"]
				IF_STACK.pop_front()
			
			# if we encounter an ENDIF, then we were coming from an ELSE and must go
			# back by one IF because we didn't pop it when we jumped
			"endif":
				IF_STACK.pop_front()
			
			"set":
				parse_expression(content, false)
			
			"transition":
				var args: Array[String]
				args.assign(content.split(" ", false))
				var trans = args.pop_front()
				
				print("Starting transition %s" % trans)
				
				SignalBus.transition.emit(trans, args)
				await SignalBus.transition_started
				
				SCRIPT.TRANS = true
			
			"scene":
				print("Changing scene to %s\n" % content)
				SignalBus.scene.emit(content)
			
			"show":
				var args := Array(content.split(" ", false))
				var image = args.pop_front()
				
				var at   : String = "origin"
				var with : String = "none"
				
				print("Adding Image %s\n" % image)
				
				var i : int = 0
				while (i < args.size()):
					match args[i]:
						"AT":
							at = args[i+1]
						"WITH":
							with = args[i+1]
					i += 1
				
				SignalBus.show.emit(image, at, with)
			
			"pause":
				print("Pausing script for %s seconds" % content)
				await get_tree().create_timer(float(content)).timeout
				print("Finished waiting\n")
			
			"wait":
				print("Pausing script. Waiting for user input to resume")
				var resume : bool = false
				while (not resume):
					resume = resume or Input.is_action_just_pressed("dialogue_next")
					await get_tree().create_timer(float(0.01)).timeout
				print("Got input, resuming script\n")
			
			"finish":
				break
			
			# if all else has failed, then the keyword is a character short handle
			# and we start a tirade
			_:
				SCRIPT.TIRADE  = true
				SCRIPT.SPEAKER = keyword

	end_script()
	return
