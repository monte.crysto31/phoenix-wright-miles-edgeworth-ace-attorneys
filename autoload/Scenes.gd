extends Node


var Characters : Dictionary[String, Resource]  = {
	# Edgy boooooy
	"EdgCourtAngry"  : preload("res://scenes/characters/court/Edgeworth/EdgCourtAngry.tscn"),
	"EdgCourtIdle"   : preload("res://scenes/characters/court/Edgeworth/EdgCourtIdle.tscn"),
	"EdgCourtOof"    : preload("res://scenes/characters/court/Edgeworth/EdgCourtOof.tscn"),
	"EdgCourtPaper"  : preload("res://scenes/characters/court/Edgeworth/EdgCourtPaper.tscn"),
	"EdgCourtPissed" : preload("res://scenes/characters/court/Edgeworth/EdgCourtPissed.tscn"),
	"EdgCourtPoint"  : preload("res://scenes/characters/court/Edgeworth/EdgCourtPoint.tscn"),
	"EdgCourtShrug"  : preload("res://scenes/characters/court/Edgeworth/EdgCourtShrug.tscn"),
	"EdgCourtSlam"   : preload("res://scenes/characters/court/Edgeworth/EdgCourtSlam.tscn"),
	"EdgCourtSmug"   : preload("res://scenes/characters/court/Edgeworth/EdgCourtSmug.tscn"),
	
	# Mr Wrong?
	"WrightCourtIdle"  : preload("res://scenes/characters/court/Wright/WrightCourtIdle.tscn"),
	"WrightCourtThink" : preload("res://scenes/characters/court/Wright/WrightCourtThink.tscn"),
}

var Places : Dictionary[String, Resource]  = {
	# Court
	"court_desk_defense"     : preload("res://scenes/places/court/court_desk_defense.tscn"),
	"court_desk_prosecution" : preload("res://scenes/places/court/court_desk_prosecution.tscn"),
	"court_desk_witness"     : preload("res://scenes/places/court/court_desk_witness.tscn"),
	"detention_center"       : preload("res://scenes/places/detention_center.tscn"),
	
	# Precinct
	
	# Wright & Co Law Offices
	"office_day"   : preload("res://scenes/places/office_day.tscn"),
	"office_night" : preload("res://scenes/places/office_night.tscn"),
}

var Positions  : Dictionary[String, Vector2] = {
	"origin" : Vector2(   0.0,    0.0),
	"center" : Vector2( 960.0, 1080.0),
}


func get_position(at:String) -> Vector2 :

	if Positions.keys().has(at):
		return Positions[at]
	
	# at is of format '(x,y)'
	var xy := Array(at.replace("(", "").replace(")", "").split(",", false))
	
	return Vector2(float(xy[0]), float(xy[1]))
