class_name Character

var short_name : String
var char_name  : String

@export_enum("MALE", "FEMALE") var what_tone = 0
var what_color : Color
var what_speed : float


func _init(key:String, data:Dictionary):
	
	self.short_name = key
	self.char_name  = data["char_name"]
	self.what_tone  = int  (data["what_tone"])
	self.what_color = Color(data["what_color"])
	self.what_speed = float(data["what_speed"])
