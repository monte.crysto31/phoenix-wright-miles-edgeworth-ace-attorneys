extends Node2D
class_name CharacterSprite


@export var char_name        : String
@export var BodyTimer        : Timer
@export var BodyAnimation    : AnimationPlayer
@export var EyeTimer         : Timer
@export var EyesAnimation    : AnimationPlayer
@export var MouthAnimation   : AnimationPlayer
@export var StartupAnimation : AnimationPlayer
@export var SkipStartup      : bool = false

var self_id : int

var eye_timer : Array[float] = [3., 6.]
var speaking  : bool         = true


func _ready() -> void:
	
	self_id = self.get_instance_id()
	
	if EyeTimer != null:
		EyeTimer.timeout.connect(animate_eyes)
	
	if StartupAnimation != null:
	
		GvnPlayer.RUNNING_ANIMATIONS.append(self_id)
		StartupAnimation.play("Startup")
		
		if SkipStartup:
			StartupAnimation.advance(StartupAnimation.get_animation("Startup").get_length() - 0.0001)
		
		await StartupAnimation.animation_finished
		GvnPlayer.RUNNING_ANIMATIONS.erase(self_id)
	
	if BodyAnimation != null:
		
		if BodyTimer != null:
			BodyTimer.timeout.connect(animate_body)
			BodyTimer.start()
		else:
			BodyAnimation.play("Body")
	
	EyeTimer.start()


# SPRITE SPECIFIC
func animate_body():
	pass


func animate_eyes():
	EyesAnimation.play("Eyes")
	await EyesAnimation.animation_finished
	EyeTimer.start(eye_timer.pick_random())


func _process(_delta: float) -> void:
	
	if MouthAnimation != null:
	
		speaking = GvnPlayer.SPEAKER != null and (GvnPlayer.SPEAKER.char_name == char_name)
		
		if speaking and not MouthAnimation.is_playing():
			MouthAnimation.play("Mouth")
		
		if GvnPlayer.SPEAKER == null or (not speaking and MouthAnimation.is_playing()):
			MouthAnimation.stop()
			MouthAnimation.play("RESET")


# in case the sprite dissapears before completing its first animation
func _exit_tree() -> void:
	GvnPlayer.RUNNING_ANIMATIONS.erase(self_id)
