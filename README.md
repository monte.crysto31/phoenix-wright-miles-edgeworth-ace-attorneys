# **Phoenix Wright & Miles Edgeworth : Ace Attorneys**
![PWMEAA logo](assets/images/title splash/vector.svg)

## **Overview**

Embark on a brand new **Ace Attorney** journey as you follow Phoenix and Edgeworth in a fan-made side story game unfolding between the events of **Trials and Tribulations** and **Apollo Justice**, and help them uncover the truth about their new compagnon in law, Aemilia Green!

A one-man team fan game project by MonteCrysto, note that it is still a WIP.

As most of the assets do not belong to me, a disclaimer has been included in the game's main menu, and this repo only contains "source code", i.e. the files needed for the Renpy launcher to compile the game.

## **Todo**

### Menus:

- [ ] Court record with profiles/evidence lists system
- [ ] Options
- [ ] Save/Load
- [ ] Music/Picture Box
- [ ] Portfolio (evidence/character/locations by case and ending lists)
- [ ] Move (requires the Location class)

### Gameplay/Mechanics:

- [ ] Screen shaking
- [ ] Zoomed mugshots
- [ ] Testimony/exam screen splashes
- [x] Godot Visual Novel scripting and parsing
- [ ] point-and-click investigations
- [ ] courtroom/testimony
- [ ] logic chess/magic joust
- [ ] maybe top-down gameplay ? (VERY OPTIONAL, GAME/PLOT CHANGING!!!)

### Plot:

- [x] Case 1
- [ ] Case 2
- [ ] Case 3
- [ ] Case 4
- [ ] Case 5
- [x] Overall plot (true culprits/deaths)
- [ ] Precise unfolding
- [ ] handle multiple endings: "Game Over", "The Fool" and "True Detective"

### New characters' sprites:

- Case 1
    - [ ] Aemilia Green (suit 1)
    - [ ] Dr. Alex Sharp
    - [ ] Dr. Daniel Sharp
    - [ ] Tyler Crook

- Case 2
    - [ ] John Barman
    - [ ] Tommy "The Gun" Thompson
    - [ ] Kunst Homer
    - [ ] Lea Garf

- Case 3
    - [ ] Alfred Payne
    - [ ] Aemilia Green (suit 2)
    - [ ] English Judge
    - [ ] Myla Abbott
    - [ ] Father Lanval

- Case 4
    - [ ] Leo Garf
    - [ ] Undercover BND agent
    - [ ] Mob man (2 or 3 variations)
    - [ ] Mafia Boss (2 suits)

- Case 5
    - [ ] Grooms (2 or 3 variations)
    - [ ] Erl King

- Other
    - [ ] Blue Knight (shield)
    - [ ] Red Knight (lance)
    - [ ] Green Knight (axe)
    - [ ] Black Knight (executionner sword)

### New locations' BG (by 1st appearance):

- Case 1
    - [ ] 3rd St. back alley
    - [ ] 3rd St. (appartment 127)

- Case 2
    - [ ] Glimmerick's Inn (downtown L.A. irish pub)
    - [ ] Glimmerick's underground cold storage
    - [ ] Glimmerick's back alley

- Case 3
    - [ ] Old Bailey front, hall and underground
    - [ ] English courtroom
    - [ ] Holy Sepulchre Church

- Case 4
    - [ ] Borscht Club (up and down)
    - [ ] Barscht Club back alley

- Case 5
    - [ ] Opera (London)
    - [ ] Glastonbury Church and Misc. (Glastonbury)
    - [ ] Others (not decided by the plot yet)

- Other
    - [ ] Make CGs when game finished for Picture Box
