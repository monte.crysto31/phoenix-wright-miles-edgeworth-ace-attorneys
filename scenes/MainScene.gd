extends Node2D


func _ready():
	
	SignalBus.transition .connect(scene_transition)
	SignalBus.scene      .connect(scene_change)
	SignalBus.show       .connect(image_add)
	SignalBus.camera_span.connect(camera_span)
	
	GvnPlayer.START("Test1")
	await GvnPlayer.script_ended
	
	print("TUTO Completed!")


func scene_transition(trans:String, args:Array[String]):
	
	%HUD.hide_dialogue_box()
	%HUD.hide_quick_menu()
	await RenderingServer.frame_post_draw
	
	Transitions.callv(trans, args)

	%HUD.show_quick_menu()


func scene_change(new_scene:String):
	
	%HUD.hide_dialogue_box()
	
	# clear the old scene
	for node in %Scene.get_children():
		%Scene.remove_child(node)
	for node in %Characters.get_children():
		%Characters.remove_child(node)
	
	# add the new one
	var tmp : Node2D = Scenes.Places[new_scene].instantiate()
	%Scene.add_child(tmp)


func image_add(new_image:String, at:String, _with:String):
	
	var tmp : Node2D = Scenes.Characters[new_image].instantiate()
	tmp.position = Scenes.get_position(at)
	%Characters.add_child(tmp)


func camera_span(x1:float, x2:float, dur:float):
	
	%Camera2D.position.x = x1
	
	var weight   : float = 0.8
	var midpoint : float = (x2 - x1) * weight
	
	var tween = get_tree().create_tween()
	
	tween.tween_property(%Camera2D, "position", Vector2(x1 + midpoint, 1), dur * pow(weight, 2.)) \
		.set_trans(Tween.TRANS_SINE) \
		.set_ease(Tween.EASE_IN)
	tween.tween_property(%Camera2D, "position", Vector2(x2, 1), dur * (1. - pow(weight, 2.))) \
		.set_trans(Tween.TRANS_SINE) \
		.set_ease(Tween.EASE_OUT)
	
	await tween.finished
	
	%Camera2D.position.x = 0.
	SignalBus.transition_finished.emit()
