extends MarginContainer


@onready var NameLabel : Label         = $VBoxContainer/Name/MarginContainer/NameLabel
@onready var Text      : RichTextLabel = $VBoxContainer/Dialogue/MarginContainer/DialogueLabel

var text_speed  : float = 0.0
var text_acc    : float = 0.0
var text_length : int   = 0

var pauses : Dictionary = {}

var SPEAKING : bool = false # is a character speaking right now?


func _ready():
	
	SignalBus.say.connect(self.start_line)


func _process(delta):
	
	if self.SPEAKING:
	
		self.text_acc -= delta
		
		# reveal one character if accumulator reaches zero
		if self.text_acc <= 0.:
			
			self.text_acc = self.text_speed
			self.Text.visible_characters += 1
		
		# finish the line if we revealed ann the characters or we clicked mid-sentence
		if self.Text.visible_characters >= self.text_length or Input.is_action_just_pressed("dialogue_next"):
			
			end_line()
	
	elif self.is_visible() and Input.is_action_just_pressed("dialogue_next"):
		
		SignalBus.next_line.emit()


# fill the pauses dictionary with str_pos -> duration where str_pos is the
# index in the line where the pause needs to happen
func dialogue_pre_pass(line:String):
	
	var old_line : String = line.strip_edges()
	var str_pos  : int    = 0
	
	self.text_length = old_line.length()
	
	while str_pos < (self.text_length-1):
		
		# - Hesitations are marked by a dash and nothing after, while a coumpound
		# word is a dash with no space before or after. The former cause a short pause
		# - Commas cause a short pause
		if (old_line.begins_with("- ") or
			old_line.begins_with(", ")):
			
			self.pauses[str_pos] = 0.2
			
			str_pos += 2
			old_line = old_line.erase(0, 2)
			
			continue
		
		# three suspension dots cause two short pauses and a long one
		elif old_line.begins_with("... "):
			
			self.pauses[str_pos]   = 0.2
			self.pauses[str_pos+1] = 0.2
			self.pauses[str_pos+2] = 0.6
			
			str_pos += 4
			old_line = old_line.erase(0, 4)
			
			continue
		
		# full stops, exclamation and interrogation marks NOT AT THE END cause a long pause
		elif (old_line.begins_with(". ") or
			old_line.begins_with("! ") or
			old_line.begins_with("? ")
			) and str_pos < (self.text_length-1):
			
			self.pauses[str_pos] = 0.6
			
			str_pos += 2
			old_line = old_line.erase(0, 2)
			
			continue
		
		else:
			
			str_pos += 1
			old_line = old_line.erase(0, 1)


func start_line(who:Character, what:String):
	
	dialogue_pre_pass(what)
	
	self.NameLabel.text = who.char_name
	
	self.Text.text = what
	self.Text.visible_characters = 0
	
	self.SPEAKING   = true
	self.text_speed = 1. / who.what_speed
	self.text_acc   = self.text_speed
	
	self.show()


func end_line():
	
	self.Text.visible_characters = -1
	self.SPEAKING = false
	GvnPlayer.SPEAKER = null
