extends TextureButton


@export var save_textures     : Array[Texture2D]
@export var slot_num_textures : Array[Texture2D]


func _ready() -> void:
	
	fetch_save(2)


func fetch_save(slot:int):
	
	$SlotNumBG.texture = slot_num_textures[slot % slot_num_textures.size()]
	$SlotNumLabel.text = str(slot) + "."


func create_save():
	
	var date_dict : Dictionary = Time.get_date_dict_from_system()
