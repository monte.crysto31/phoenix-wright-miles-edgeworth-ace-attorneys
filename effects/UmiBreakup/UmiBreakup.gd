extends Node2D


@export var base_texture : Texture2D
@export var square_scale : int   = 5
@export var move_speed   : float = 800.
@export var duration     : float = 4.
@export_range(0., 1.) var move_ratio : float = 0.666
@export var base_noise    : Texture2D
@export var base_gradient : Texture2D


# the base square size is 120px for 16x9 because of the aspect ratio
var screen_size : Vector2  = Vector2i(1920., 1080.)
@onready var square_grid : Vector2i = Vector2i(16, 9) * self.square_scale

# info for how to send the squares flying
var part_lifetime  : float = self.duration / 2.
var offsets_range  : float = PI/2.
var disp_angle     : float
var displacements1 : Array[Vector2]
var displacements2 : Array[Vector2]
var gradient1      : Image
var gradient2      : Image

# managing the effect
@onready var image_noise    : Image = self.base_noise   .get_image()
@onready var image_gradient : Image = self.base_gradient.get_image()
var acc   : float = 0.0 # accumulator during the effect
var state : int   = 0   # 0 -> nothing, 1 -> effect, 2 -> reset and stop


# polygon_id is either 0 -> polygon1 or 1 -> polygon2
func fill_polygons(polygon_id:int):
	
	var polygon_nodes = [$Polygon1, $Polygon2]
	var tmp_polygon   = PackedVector2Array([])
	var sq_size   : float   = screen_size.x / float(self.square_grid.x)
	var sq_offset : Vector2 = Vector2.ONE * sq_size / 2.
	
	var sq_pos : Vector2
	
	for y in (self.square_grid.y + polygon_id):
		for x in (self.square_grid.x + polygon_id):
			
			# fill polygon and polygons
			
			sq_pos = Vector2(x, y) * sq_size
			
			tmp_polygon.append_array([
				sq_pos - sq_offset * polygon_id,
				sq_pos - sq_offset * polygon_id + Vector2.RIGHT   * sq_size,
				sq_pos - sq_offset * polygon_id + Vector2(1., 1.) * sq_size,
				sq_pos - sq_offset * polygon_id + Vector2.DOWN    * sq_size])
			
			polygon_nodes[polygon_id].polygons.append([
				(x + y * (self.square_grid.x + polygon_id)) * 4,
				(x + y * (self.square_grid.x + polygon_id)) * 4 + 1,
				(x + y * (self.square_grid.x + polygon_id)) * 4 + 2,
				(x + y * (self.square_grid.x + polygon_id)) * 4 + 3])
	
	polygon_nodes[polygon_id].set_polygon(tmp_polygon)
	polygon_nodes[polygon_id].set_uv     (tmp_polygon)


func fill_gradients():
	
	self.gradient1 = Image.create(self.square_grid.x  , self.square_grid.y  , false, Image.FORMAT_RGBA8)
	self.gradient2 = Image.create(self.square_grid.x+1, self.square_grid.y+1, false, Image.FORMAT_RGBA8)
	
	var grad1 : float
	var grad2 : float
	
	for y in (self.square_grid.y + 1):
		for x in (self.square_grid.x + 1):
			
			grad2 = 0.1 + self.image_gradient.get_pixel(x, y).r \
				- self.image_noise.get_pixel(self.square_grid.x - x, self.square_grid.y - y).r * 0.1
			
			self.gradient2.set_pixel(x, y, Color(grad2, grad2, grad2))
			
			if x == self.square_grid.x or y == self.square_grid.y:
				continue
			
			grad1 = 0.1 + self.image_gradient.get_pixel(x, y).r - self.image_noise.get_pixel(x, y).r * 0.1
			
			self.gradient1.set_pixel(x, y, Color(grad1, grad1, grad1))


func fill_displacements(polygon_id:int):
	
	var disp : Vector2
	
	for y in (self.square_grid.y + polygon_id):
		for x in (self.square_grid.x + polygon_id):
			
			disp = Vector2.from_angle(self.disp_angle + randf() * self.offsets_range - self.offsets_range/2.) * self.move_speed
			
			match polygon_id:
				0:
					self.displacements1.append_array([disp, disp, disp, disp])
				_:
					self.displacements2.append_array([disp, disp, disp, disp])


func _ready():
	
	# texture for Polygon1 is good, the one for Polygon2 has to be resized
	$Polygon1.texture = self.base_texture
	$Polygon2.texture = self.base_texture
	
	# generate polygons
	
	fill_polygons(0)
	fill_polygons(1)
	
	# generate images
	
	self.image_noise   .resize(self.square_grid.x + 1, self.square_grid.y + 1, Image.INTERPOLATE_NEAREST)
	self.image_gradient.resize(self.square_grid.x + 1, self.square_grid.y + 1, Image.INTERPOLATE_NEAREST)
	
	$Polygon1.material.set("shader_parameter/grid_size", self.square_grid)
	$Polygon2.material.set("shader_parameter/grid_size", self.square_grid - Vector2i.ONE)
	
	$Polygon1.material.set("shader_parameter/duration", self.duration)
	$Polygon2.material.set("shader_parameter/duration", self.duration)
	
	$Polygon1.material.set("shader_parameter/move_ratio", self.move_ratio)
	$Polygon2.material.set("shader_parameter/move_ratio", self.move_ratio)
	
	$Polygon2.material.set("shader_parameter/offset",
		Vector2(0.5 / float(self.square_grid.x), 0.5 / float(self.square_grid.y)))


# send the squares flying in a given direction, one of 0, 1, 2 or 3
func burst(direction:int):

	# generate rotation offsets for a more natural effect
	
	self.disp_angle = (2*direction-3) * PI/4.
	
	match direction:
		0:
			self.image_gradient.flip_x()
		2:
			self.image_gradient.flip_y()
		3:
			self.image_gradient.flip_x()
			self.image_gradient.flip_y()
	
	fill_displacements(0)
	fill_displacements(1)
	
	fill_gradients()
	
	$Polygon1.material.set("shader_parameter/gradient", ImageTexture.create_from_image(self.gradient1))
	$Polygon2.material.set("shader_parameter/gradient", ImageTexture.create_from_image(self.gradient2))
	
	$Polygon1.material.set("shader_parameter/start_time", Time.get_ticks_msec() / 1000.)
	$Polygon2.material.set("shader_parameter/start_time", Time.get_ticks_msec() / 1000.)
	
	self.state = 1


func _process(delta):
	
	match self.state:
		
		2:
			$Polygon1.material.set("shader_parameter/start_time", -1.)
			$Polygon2.material.set("shader_parameter/start_time", -1.)
			self.hide()
			self.state = 0
			
			# small pause to not rush things
			await get_tree().create_timer(0.5).timeout
			
			SignalBus.transition_finished.emit()
			
			self.queue_free()
		
		1:
			var tmp_poly1  = Array($Polygon1.polygon)
			var tmp_poly2  = Array($Polygon2.polygon)
			var nb_poly1   = self.displacements1.size()
			var nb_poly2   = self.displacements2.size()
			
			var poly_id : int
			var grad : float
			
			for i in nb_poly1:
				
				poly_id = floori(float(i) / 4.)
				grad = self.gradient1.get_pixel(
					poly_id % self.square_grid.x,
					floori(float(poly_id) / float(self.square_grid.x))
				).r
				
				if self.acc >= self.move_ratio * grad * self.duration:
					tmp_poly1[i] += self.displacements1[i] * delta
			
			for i in nb_poly2:
				
				poly_id = int(float(i) / 4.)
				grad = self.gradient2.get_pixel(
					poly_id % (self.square_grid.x+1),
					floori(float(poly_id) / float(self.square_grid.x+1))
				).r
				
				if self.acc >= self.move_ratio * grad * self.duration:
					tmp_poly2[i] += self.displacements2[i] * delta
			
			$Polygon1.set_polygon(tmp_poly1)
			$Polygon2.set_polygon(tmp_poly2)
			
			self.acc += delta
			
			if self.acc > self.duration:
				self.acc   = 0.
				self.state = 2
