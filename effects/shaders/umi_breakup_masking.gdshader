shader_type canvas_item;


// given/changed when we start
uniform sampler2D gradient;
uniform sampler2D breakup_cells;
uniform vec2      grid_size;
uniform float     duration;
uniform float     move_ratio;
uniform float     start_time = -1.;
uniform vec2      offset = vec2(0., 0.);

// constant
uniform float cell_forms     = 12.;
uniform float pre_move_forms =  9.;


void fragment()
{
	vec2 true_UV = UV - offset;

	vec2 true_size = grid_size;
	if (offset.x != 0.) {
		true_size += vec2(1., 1.);
	}

	// this value dictates when we start to fly off
	float grad = texture(
		gradient,
		floor(true_UV * true_size) / true_size + 0.5 / true_size).r;

	// the elapsed time since 'start_time'
	float progress = (TIME - start_time);

	// 1. the tile we are part of shrinks after 'shrink_time' seconds, and shrinks for 'part_lifedur'
	// 2. there will be two transformation steps done in place, THEN the part moves (so it's already rounded when it flies off)
	// 3. shrink_time + part_lifedur = duration so that the last particle has died before the effect ends, with 'pre_move_forms' steps' worth of time to spare
	float part_lifedur = duration * (1. - move_ratio);
	float move_time    = (grad - 0.1) * duration * move_ratio; // slight offset to ensure no particle is left
	float shrink_time  = move_time - (pre_move_forms / cell_forms) * part_lifedur;

	// 'start_time' equals -1. when we haven't started the effect yet, so we
	// wait until it is positive to keep track of the progress
	if (start_time > 0. && (progress > shrink_time))
	{
		// the index of the current cell, dictated by the progress, from 0 to 15
		int cell_radius = int(cell_forms * min(1, (progress - shrink_time)/(part_lifedur)));

		// this is the position of the current pixel at coordinates UV within
		// the current tile
		vec2 cell_uv = vec2(
			mod(true_UV.x, 1. / true_size.x) * true_size.x / cell_forms + float(cell_radius) * 1. / cell_forms,
			mod(true_UV.y, 1. / true_size.y) * true_size.y
		);

		COLOR = vec4(
			texture(TEXTURE, UV).rgb,
			texture(TEXTURE, UV).a * texture(breakup_cells, cell_uv).r
		);
	}
	else
	{
		//COLOR = vec4(grad, grad, grad, 1.);
		COLOR = texture(TEXTURE, UV);
	}
}